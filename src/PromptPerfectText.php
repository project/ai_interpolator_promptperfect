<?php

namespace Drupal\ai_interpolator_promptperfect;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * PromptPerfect Text field.
 */
class PromptPerfectText extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The PromptPerfect requester.
   */
  public PromptPerfect $promptperfect;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\ai_interpolator_promptperfect\PromptPerfect $promptperfect
   *   The PromptPerfect requester.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   *   The logger channel interface.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityManager,
    PromptPerfect $promptperfect,
    FileSystemInterface $fileSystem,
    FileRepositoryInterface $fileRepo,
    Token $token,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerChannel,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entityManager;
    $this->promptperfect = $promptperfect;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
    $this->currentUser = $currentUser;
    $this->loggerChannel = $loggerChannel;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('ai_interpolator_promptperfect.api'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $form['interpolator_promptperfect_target_model'] = [
      '#type' => 'select',
      '#title' => 'Target Model',
      '#required' => TRUE,
      '#options' => [
        'chatgpt' => $this->t('Chat GPT'),
        'gpt4' => $this->t('GPT-4'),
        'stablelm-tuned-alpha-7b' => $this->t('StabelLM Tuned Alpha 7b'),
        'claude' => $this->t('Claude'),
        'claude-2' => $this->t('Claude 2'),
        'ernie-bot-turbo' => $this->t('Ernie Bot Turbo'),
        'llama-2-70b-chat' => $this->t('Llama 2 70b chat'),
        'llama-2-13b-chat' => $this->t('Llama 2 13b chat'),
        'llama-2-7b-chat' => $this->t('Llama 2 7b chat'),
        'cogenerate' => $this->t('CoGenerate'),
        'jinachat' => $this->t('JinaChat'),
        'text-davinci-003' => $this->t('GPT-3 (Text Davinci 003)'),
        'dalle-3' => $this->t('Dall-E 3'),
        'dalle' => $this->t('Dall-E'),
        'sdxl' => $this->t('Stable Diffusion XL'),
        'midjourney' => $this->t('Midjourney'),
        'sd' => $this->t('Stable Diffusion'),
        'kadinsky' => $this->t('Kadinsky'),
        'dreamshaper' => $this->t('DreamShaper'),
        'absolutereality' => $this->t('Absolute Reality'),
        'anything' => $this->t('Anything AI'),
        'deliberate' => $this->t('deliberate.ai'),
        'sdxl-emoji' => $this->t('Stable Diffusion XL Emoji'),
        'lexica' => $this->t('Lexica'),
      ],
      '#description' => $this->t('The target model to use.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_promptperfect_target_model', 'chatgpt'),
    ];

    $form['interpolator_promptperfect_iterations'] = [
      '#type' => 'number',
      '#title' => 'Iterations',
      '#description' => $this->t('The number of iterations to be used for the prompt optimization. Larger number of iterations will result in better optimization, but it will take longer time.'),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_promptperfect_iterations', 1),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig, $delta = 0) {
    $model = $interpolatorConfig['promptperfect_target_model'];
    $params = [];
    if (!empty($interpolatorConfig['promptperfect_iterations'])) {
      $params['iterations'] = $interpolatorConfig['promptperfect_iterations'];
    }
    $values = [];
    foreach ($entity->{$interpolatorConfig['base_field']} as $value) {
      if ($value->value) {
        $return = $this->promptperfect->promptPerfect($value->value, $model, $params);
        if (!empty($return['result']['promptOptimized'])) {
          $values[] = $return['result']['promptOptimized'];
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Should be a string.
    if (!is_string($value)) {
      return FALSE;
    }
    // Otherwise it is ok.
    return TRUE;
  }

}
