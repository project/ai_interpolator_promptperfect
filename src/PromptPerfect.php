<?php

namespace Drupal\ai_interpolator_promptperfect;

use Drupal\ai_interpolator_promptperfect\Form\PromptPerfectConfigForm;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;

/**
 * PromptPerfect API creator.
 */
class PromptPerfect {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * API Key.
   */
  private string $apiKey;

  /**
   * The base path.
   */
  private string $basePath = 'https://api.promptperfect.jina.ai/';

  /**
   * Constructs a new PromptPerfect object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(Client $client, ConfigFactory $configFactory) {
    $this->client = $client;
    $this->apiKey = $configFactory->get(PromptPerfectConfigForm::CONFIG_NAME)->get('api_key') ?? '';
  }

  /**
   * Generate a better prompt.
   *
   * @param string $prompt
   *   The prompt to improve.
   * @param string $model
   *   The model to use.
   * @param array $config
   *   Other configs.
   *
   * @return string
   *   The description.
   */
  public function promptPerfect($prompt, $model, $config = []) {
    if (!$this->apiKey || !$prompt || !$model) {
      return [];
    }
    $data['data'] = $config;
    $data['data']['prompt'] = $prompt;
    $data['data']['targetModel'] = $model;

    $guzzleOptions['headers']['Content-Type'] = 'application/json';
    return json_decode($this->makeRequest("optimize", [], 'POST', json_encode($data), $guzzleOptions)->getContents(), TRUE);
  }

  /**
   * Make PromptPerfect call.
   *
   * @param string $path
   *   The path.
   * @param array $query_string
   *   The query string.
   * @param string $method
   *   The method.
   * @param string $body
   *   Data to attach if POST/PUT/PATCH.
   * @param array $options
   *   Extra headers.
   *
   * @return string|object
   *   The return response.
   */
  protected function makeRequest($path, array $query_string = [], $method = 'GET', $body = '', array $options = []) {
    // We can wait some.
    $options['connect_timeout'] = 30;
    $options['read_timeout'] = 30;
    // Don't let Guzzle die, just forward body and status.
    $options['http_errors'] = FALSE;
    // Headers.
    $options['headers']['x-api-key'] = 'token ' . $this->apiKey;

    if ($body) {
      $options['body'] = $body;
    }

    $new_url = $this->basePath . $path;
    $new_url .= count($query_string) ? '?' . http_build_query($query_string) : '';

    $res = $this->client->request($method, $new_url, $options);

    return $res->getBody();
  }

}
