<?php

namespace Drupal\ai_interpolator_promptperfect\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator_promptperfect\PromptPerfectText;

/**
 * The rules for a string long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_promptperfect_string_long",
 *   title = @Translation("Prompt Perfect Prompt"),
 *   field_rule = "string_long",
 * )
 */
class PromptPerfectStringLong extends PromptPerfectText {

  /**
   * {@inheritDoc}
   */
  public $title = 'PromptPerfect Prompt';

}
