<?php

namespace Drupal\ai_interpolator_promptperfect\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator_promptperfect\PromptPerfectText;

/**
 * The rules for a text long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_promptperfect_text_long",
 *   title = @Translation("Prompt Perfect Prompt"),
 *   field_rule = "text_long",
 * )
 */
class PromptPerfectTextLong extends PromptPerfectText {

  /**
   * {@inheritDoc}
   */
  public $title = 'Prompt Perfect Prompt';

}
